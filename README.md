# Media Menu #

### Demo ###

[https://media-menu-samg-01.firebaseapp.com/](https://media-menu-samg-01.firebaseapp.com/)

### About ###

#### Carousel ####
* UI uses [Flickity](https://flickity.metafizzy.co/) Carousel as a Component Directive Wrapper
* Refer to The [Flikity Directive Component Wrapper](https://bitbucket.org/robsedge/media-menu/src/master/src/app/components/carousel/carousel.flickity.html)
* Aim for an Unobtrusive 3rd party Lib usage for future deprication/updgrade (template and config changes only)

### TODO ###

#### TODO UI ####
* Timestamp overlay on images 
* Badge icon overlay on images 
* Carousel Arrows position on right
* Carousel Width scales ( would like to demo a more fluid sizing )
* Main Title spacing
* 'View all' link in header
* 'Dummy' Links on each carousel item for future linking to full detail
* Hover hand states
* ALLy considerations
* End To End / Cross Browser / Device Tests
* Final Unit Tests

#### TODO Tooling and Pipelines ####

* SonarCube to CI
* PR Checks 
* Trigger deployment from Release Tags

### Running/Development ###

* `npm install -g @angular/cli`
* `npm run start`  or `ng serve --open`
*  Running: [http://localhost:4200/](http://localhost:4200/) 

### Tests ###

* Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
* Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Development: Component Library | Styleguide ###

* Build StyleGuide of available Components - `npm run storybook` ( currently building `.stories.ts` from `app/components`)
* Run `npm run storybook` and view at [http://localhost:6006/](http://localhost:6006/)

### CI ###

* Unit Tests and E2E pipeline: [CircleCI](https://app.circleci.com/pipelines/bitbucket/robsedge/media-menu)

* Firebase Deploy `media-menu-samg-01` refer to `firebase.json` uses `firebase cli`
import { Component, OnInit } from '@angular/core';
import { SpflLatestVideosService } from './spfl-latest-videos.service';

import { mediaPreviewItem } from '../types/media-preview-item';

@Component({
  selector: 'app-spfl-latest-videos',
  templateUrl: './spfl-latest-videos.feature.html'
})
export class SpflLatestVideos implements OnInit {
  items: mediaPreviewItem[];

  constructor(private service: SpflLatestVideosService ) {}

  ngOnInit(): void {
    this.fetchItems();
  }

  setItems = (items: mediaPreviewItem[]): void =>{
    this.items = items;
  }

  fetchItems(): void {
    this.service.get().subscribe(this.setItems);
  }

}

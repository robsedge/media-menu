/* TODO 
    - FEED Validation / contract test of return 
    - Harder Guard on `sections[0]` selector  - factory.validate()
*/

import { spflVideos, spflVideoItem } from './spfl-videos';
import { mediaPreviewItem } from '../types/media-preview-item';

const factory = {
    make: {
        mediaPreview: (spflVideo: spflVideoItem): mediaPreviewItem => {
            return {
                id: spflVideo.id,
                thumbnail: spflVideo.mediaData.thumbnailUrl,
                title: spflVideo.metaData.title,
                duration: spflVideo.metaData.VideoDuration,
                category: spflVideo.metaData.corecategories[0] ?? ''
            }
        },
        mediaPreviews: (spflVideoItems: spflVideoItem[]): mediaPreviewItem[] => {
            let mediaPreviewItems = [];
            spflVideoItems.forEach( (spflVideo: spflVideoItem)=> {
                const mediaPreview: mediaPreviewItem = factory.make.mediaPreview(spflVideo)
                mediaPreviewItems.push(mediaPreview)
            })
            return mediaPreviewItems;
        }
    },
    map: (spflVideosData: spflVideos): mediaPreviewItem[] => {
        const spflVideoItems: spflVideoItem[] = spflVideosData.sections[0].itemData;
        return factory.make.mediaPreviews(spflVideoItems);
    },
    validate: (spflVideosData: spflVideos): boolean => {
        return spflVideosData.sections.length ? spflVideosData.sections[0].itemData.length > 0 : false;
    }
};

export default factory;

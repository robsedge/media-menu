import factory from './spfl-latest-videos.feed.factory';

import { spflVideos, spflVideoItem } from './spfl-videos';
import { mediaPreviewItem } from '../types/media-preview-item';

describe('FeedFactory: Unit Tests', ()=> { 
    const mockFeed: spflVideos = {
        sections: [
            {
                itemData: [
                    {
                        id: 'one',
                        mediaData: {
                            thumbnailUrl: './assets/images/football.jpg'
                        },
                        metaData: {
                            corecategories: [ 'MockCategory' ],
                            VideoDuration: 100,
                            title: 'MockTitle',
                        }
                    }
                ]
            }
        ]
    };
    const mockMediaPreviewOne: mediaPreviewItem = { id: '1', thumbnail: '1', title: '1', duration: 1, category: '1' };
    const mockMediaPreviewTwo: mediaPreviewItem = { id: '1', thumbnail: '1', title: '1', duration: 1, category: '1' };
    const mockMediaPreviewItems: mediaPreviewItem[] = [ mockMediaPreviewOne, mockMediaPreviewTwo ];

    it('map', ()=> {
        spyOn(factory.make, 'mediaPreviews').and.returnValue(mockMediaPreviewItems);

        const result = factory.map(mockFeed);

        expect(result).toEqual(mockMediaPreviewItems);
        expect(factory.make.mediaPreviews).toHaveBeenCalledWith(mockFeed.sections[0].itemData);
    });

    it('make.mediaPreviews', ()=> {
        const mockitemData: spflVideoItem[] = [
            {
                id: 'one',
                mediaData: {
                    thumbnailUrl: 'mockImageOne'
                },
                metaData: {
                    corecategories: [ 'MockCategoryOne' ],
                    VideoDuration: 100,
                    title: 'MockTitleOne',
                }
            }
        ];

        const mockMediaPreviewOne: mediaPreviewItem = {
            id: '1',
            thumbnail: '1',
            title: '1',
            duration: 1,
            category: '1'
        };

        const expected = [mockMediaPreviewOne];

        spyOn(factory.make, 'mediaPreview').and.returnValue(mockMediaPreviewOne);

        expect(factory.make.mediaPreviews(mockitemData)).toEqual(expected);
        expect(factory.make.mediaPreview).toHaveBeenCalledWith(mockitemData[0]);
    });

    describe('make.mediaPreview', ()=> {
        it('has Category', ()=> {
            const mockItemData: spflVideoItem =  {
                id: 'one',
                mediaData: {
                    thumbnailUrl: './assets/images/football.jpg'
                },
                metaData: {
                    corecategories: [ 'MockCategory' ],
                    VideoDuration: 100,
                    title: 'MockTitle',
                }
            };
                        
            const expected: mediaPreviewItem = {
                id: mockItemData.id,
                thumbnail: mockItemData.mediaData.thumbnailUrl,
                title: mockItemData.metaData.title,
                duration: mockItemData.metaData.VideoDuration,
                category: mockItemData.metaData.corecategories[0]
            };
    
            expect(factory.make.mediaPreview(mockItemData)).toEqual(expected);
        });
        it('has 0 Category', ()=> {
            const mockItemData: spflVideoItem =  {
                id: 'one',
                mediaData: {
                    thumbnailUrl: './assets/images/football.jpg'
                },
                metaData: {
                    corecategories: [],
                    VideoDuration: 100,
                    title: 'MockTitle',
                }
            };
                        
            const expected: mediaPreviewItem = {
                id: mockItemData.id,
                thumbnail: mockItemData.mediaData.thumbnailUrl,
                title: mockItemData.metaData.title,
                duration: mockItemData.metaData.VideoDuration,
                category: ''
            };
    
            expect(factory.make.mediaPreview(mockItemData)).toEqual(expected);
        });
    });

    describe('validate', ()=> {
        it('valid', ()=> {
            const mockspflVideos: spflVideos = {
                sections: [
                    {
                        itemData: [
                            {
                                id: 'mockId',
                                mediaData: {
                                    thumbnailUrl: 'mockThumbnail'
                                },
                                metaData: {
                                    corecategories: [ 'MockCategory' ],
                                    VideoDuration: 100,
                                    title: 'MockTitle',
                                }
                            }
                        ]
                    }
                ]
            };
            expect(factory.validate(mockspflVideos)).toBe(true);
        });

        it('invalid - no itemData', ()=> {
            const mockspflVideos: spflVideos = { sections: [ { itemData: [] } ] };
            expect(factory.validate(mockspflVideos)).toBe(false);
        });

        it('invalid - no section', ()=> {
            const mockspflVideos: spflVideos = { sections: [] };
            expect(factory.validate(mockspflVideos)).toBe(false);
        });
    });
});


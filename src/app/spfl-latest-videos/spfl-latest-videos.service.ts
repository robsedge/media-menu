import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiService } from '../api.service';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import config from './spfl-latest-videos.config';
import factory from './spfl-latest-videos.feed.factory';
import { spflVideos, spflVideoItem } from './spfl-videos';
import { mediaPreviewItem } from '../types/media-preview-item';

@Injectable({ providedIn: 'root' })
export class SpflLatestVideosService {
  endpoint;
  config = config;
  factory = factory;
  environment = environment;

  map = map;
  catchError = catchError;
  throwError = throwError;

  constructor(private api: ApiService) { 
    this.setEndPoint();
  }

  setEndPoint() {
    this.endpoint = this.environment.production? this.config.endpoints.production : this.config.endpoints.development;
  }

  responseHandler = (spflVideoData: spflVideos) => {
    return this.factory.validate(spflVideoData) ? this.success(spflVideoData) : this.fail();
  }

  success = (spflVideoData: spflVideos): mediaPreviewItem[] => { 
    const mediaPreviewItems: mediaPreviewItem[] = this.factory.map(spflVideoData);
    return mediaPreviewItems;
  }

  fail = () => { return this.throwError( this.config.messages.error ); }
  
  get() {
    return this.api.get({url: this.endpoint}).pipe( this.map( this.responseHandler ), this.catchError( this.fail ) );
  }
}

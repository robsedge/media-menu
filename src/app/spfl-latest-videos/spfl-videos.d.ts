export interface mediaData {
    mediaType?: String,
    entryId?: String,
    entryStatus?: String,
    thumbnailUrl: String
}

export interface metaData {
    body?: String,
    corecategories: String[],
    teams?: String[],
    tvshowcategories?: String[],
    SysEntryEntitlements?: String[],
    VideoDuration: Number,
    people?: String[],
    title: String,
    contenttags?: String[],
    tags?: String[]
}

export interface publicationData {
    createdAt: String,
    updatedAt: String,
    released: Boolean,
    releaseFrom: String,
    releaseTo: String,
}

export interface feedMetaData {
    id: String,
    name?: String,
    title: String,
    description?: String,
    target?: String
}

export interface spflVideoItem {
    id: String,
    mediaData: mediaData,
    metaData: metaData,
    publicationData?: publicationData
}

export interface section {
    id?: String,
    name?: String,
    itemData?: spflVideoItem[]
}

export interface spflVideos {
    feedMetaData?: feedMetaData,
    sections: section[]
}

/*
    10-09-2020
    https://thefa-cm.streamamg.com/api/v1/dc46fdad-f1f8-446f-ab50-ac44b0d77b10/fG4YqyOgfLbTvnQTjRC7YGFOFwt0BNjObLkTUvJxc6EpPK1tGC/ec6e745e-6192-4685-b69e-cb32f8d06780/en/feed/c20317ee-5a12-4142-9788-fb4232f2632a/sections/search?pageSize=15

{
    "feedMetaData": {
        "id": "c20317ee-5a12-4142-9788-fb4232f2632a",
        "name": "features_-_web",
        "title": "Features - Web",
        "description": "<p>Features - Web<br></p>",
        "target": "website"
    },
    "sections": [
        {
            "id": "96ea9424-5a63-4cb1-b7ce-3f2c5264829c",
            "name": "Features - Web",
            "itemData": [
                {
                    "id": "bb6909e8-e299-4667-b0ed-b6ec337fe42a",
                    "mediaData": {
                        "mediaType": "OnDemand",
                        "entryId": "0_zbh00h5r",
                        "entryStatus": "Ready",
                        "thumbnailUrl": "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_zbh00h5r/"
                    },
                    "metaData": {
                        "body": null,
                        "corecategories": [
                            "Feature"
                        ],
                        "teams": [],
                        "tvshowcategories": [],
                        "SysEntryEntitlements": [],
                        "VideoDuration": 659,
                        "people": [
                            "Demi Stokes"
                        ],
                        "title": "Demi Stokes Virtual Session with Lucozade Sport | Lionesses",
                        "contenttags": [
                            "Lionesses"
                        ],
                        "tags": [
                            "demi stokes",
                            "virtual sessions"
                        ]
                    },
                    "publicationData": {
                        "createdAt": "2020-09-09T08:58:36",
                        "updatedAt": "2020-09-09T09:08:39",
                        "released": true,
                        "releaseFrom": "2020-09-09T09:45:00",
                        "releaseTo": "9999-12-31T23:59:59.9999999"
                    }
                }, .... 

*/
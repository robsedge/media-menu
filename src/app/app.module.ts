import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FlickityModule } from 'ngx-metafizzy-flickity';

import { AppComponent } from './app.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { CarouselDirectiveComponent } from './components/carousel/carousel.directive.component';
import { HeadingThreeComponent } from './components/heading-three/heading-three.component';
import { HeadingStrapComponent } from './components/heading-strap/heading-strap.component';
import { PreviewMediaComponent } from './components/preview-media/preview-media.component';
import { MediaTimeStampComponent } from './components/media-time-stamp/media-time-stamp.component';
import { MediaPassIconComponent } from './components/media-pass-icon/media-pass-icon.component';
import { HeadingTwoComponent } from './components/heading-two/heading-two.component';
import { SpflLatestVideos } from './spfl-latest-videos/spfl-latest-videos.feature';

import { ApiService } from './api.service';
import { SpflLatestVideosService } from './spfl-latest-videos/spfl-latest-videos.service';

@NgModule({
  declarations: [
    AppComponent,
    CarouselComponent,
    CarouselDirectiveComponent,
    HeadingThreeComponent,
    HeadingStrapComponent,
    PreviewMediaComponent,
    MediaTimeStampComponent,
    MediaPassIconComponent,
    HeadingTwoComponent,
    SpflLatestVideos
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FlickityModule
  ],
  providers: [ ApiService, SpflLatestVideosService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

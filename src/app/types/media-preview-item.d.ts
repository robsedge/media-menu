export interface mediaPreviewItem {
    id: String,
    thumbnail: String,
    title: String,
    duration: Number,
    category: String
}

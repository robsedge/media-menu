import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface apiConfig {
  url: string
};

@Injectable({ providedIn: 'root' })
export class ApiService {
  constructor(private http: HttpClient) { }

  get(config: apiConfig) {
    return this.http.get(config.url);
  }
}

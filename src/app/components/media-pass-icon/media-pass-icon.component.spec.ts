import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaPassIconComponent } from './media-pass-icon.component';

describe('MediaPassIconComponent', () => {
  let component: MediaPassIconComponent;
  let fixture: ComponentFixture<MediaPassIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MediaPassIconComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaPassIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

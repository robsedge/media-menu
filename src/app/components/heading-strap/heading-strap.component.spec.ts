import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadingStrapComponent } from './heading-strap.component';

describe('HeadingStrapComponent', () => {
  let component: HeadingStrapComponent;
  let fixture: ComponentFixture<HeadingStrapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeadingStrapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadingStrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { action } from '@storybook/addon-actions';
import { HeadingStrapComponent } from './heading-strap.component';
export default {
  title: 'Components',
};
export const HeadingStrap = () => ({
  component: HeadingStrapComponent,
  props: {
    label: 'Heading Strap Label'
  },
});
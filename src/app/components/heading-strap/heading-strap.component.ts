import { Component, Input} from '@angular/core';

@Component({
  selector: 'app-heading-strap',
  templateUrl: './heading-strap.component.html',
  styleUrls: ['./heading-strap.component.scss']
})
export class HeadingStrapComponent {
  @Input() label: String;
  constructor() { }
}

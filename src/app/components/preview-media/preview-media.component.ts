import { Component, Input} from '@angular/core';

@Component({
  selector: 'app-preview-media',
  templateUrl: './preview-media.component.html',
  styleUrls: ['./preview-media.component.scss']
})
export class PreviewMediaComponent {
  @Input() image: String;
  @Input() alt: String;
  constructor() { }
}

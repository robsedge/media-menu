import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaTimeStampComponent } from './media-time-stamp.component';

describe('MediaTimeStampComponent', () => {
  let component: MediaTimeStampComponent;
  let fixture: ComponentFixture<MediaTimeStampComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MediaTimeStampComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaTimeStampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

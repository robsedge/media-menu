import { Component, Input } from '@angular/core';
import { mediaPreviewItem } from '../../types/media-preview-item';
@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent {
  @Input() items: mediaPreviewItem[];
  constructor() { }
  
}

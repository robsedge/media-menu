import { action } from '@storybook/addon-actions';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CarouselComponent } from './carousel.component';
import { CarouselDirectiveComponent } from './carousel.directive.component';
import { HeadingThreeComponent } from '../heading-three/heading-three.component';
import { HeadingStrapComponent } from '../heading-strap/heading-strap.component';
import { PreviewMediaComponent } from '../preview-media/preview-media.component';
import { MediaTimeStampComponent } from '../media-time-stamp/media-time-stamp.component';
import { MediaPassIconComponent } from '../media-pass-icon/media-pass-icon.component';
import { HeadingTwoComponent } from '../heading-two/heading-two.component';

export default {
  title: 'Components',
};
export const Carousel = () => ({
  component: CarouselComponent,
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  props: {
    items: [
        {
          id: "bb6909e8-e299-4667-b0ed-b6ec337fe42a",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_zbh00h5r/",
          title: "Demi Stokes Virtual Session with Lucozade Sport | Lionesses",
          duration: 659,
          category: "Feature"
        },
        {
          id: "bf7286e3-4c6c-44de-be54-fd80fc511188",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_43bxow7n/",
          title: "Women's Football Show | Matchday One",
          duration: 1798,
          category: "Feature"
        },
        {
          id: "c27bfefa-5dc6-4415-a9e3-ebe558e61f13",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_nurvrt7a/",
          title: "Mary Earps Virtual Session with Lucozade Sport | Lionesses",
          duration: 635,
          category: "Feature"
        },
        {
          id: "dd965521-865d-4ea0-a841-4a1889849b4d",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_qnf861u0/",
          title: "Gameday Show | Episode 5 | Leanne Kiernan & Sophie Haywood",
          duration: 1299,
          category: "Feature"
        },
        {
          id: "c0383ccb-e77b-4bde-97f3-69679f5e5b60",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_oilrl2ir/",
          title: "Gameday Show | Episode 4 | Erin Cuthbert & Lisa Evans",
          duration: 1406,
          category: "Feature"
        },
        {
          id: "3824b094-9026-4449-8325-26271abec280",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_k84z92ke/",
          title: "Lionesses | Coffee Club | The Best Moments",
          duration: 1959,
          category: "Feature"
        },
        {
          id: "fda6e3e7-1929-4ac7-810f-481c549e34c2",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_vf0ocshd/",
          title: "Karen Carney & Rachel Yankee Pick Their Lionesses Dream Team",
          duration: 1212,
          category: "Feature"
        },
        {
          id: "e98e2b7b-f6bb-4986-973f-14781a4d3295",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_0t43hd86/",
          title: "Gameday Show | Episode 2 | Lucy Quinn & Hannah Hampton",
          duration: 1019,
          category: "Feature"
        },
        {
          id: "e3471da1-c02e-4f97-8a24-0c836477fafa",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_hm5zqbk9/",
          title: "End of Season Awards Ceremony | Barclays FA WSL 2019/20",
          duration: 462,
          category: "Feature"
        },
        {
          id: "d726889b-659e-4bdc-9197-02e9285dd85b",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_6l20bl4o/",
          title: "Gameday Show | Episode 1 | Fliss Gibbons & Grace Maloney",
          duration: 1125,
          category: "Feature"
        },
        {
          id: "34d0276b-259b-49a5-ad0b-822ec5fce45c",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_1yhbng5r/",
          title: "One Year On | England vs USA | Steph Houghton,  Leah Williamson, Georgia Stanway & Lucy Bronze",
          duration: 376,
          category: "Feature"
        },
        {
          id: "2a4d9585-01c8-4722-978c-69a04b96ef67",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_h5p41mhm/",
          title: "One Year On | England vs Norway | Lucy Bronze",
          duration: 821,
          category: "Feature"
        },
        {
          id: "92cf67c1-bf61-4cc4-9b65-cd6cc1552ce2",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_5plf6qve/",
          title: "Lionesses | Coffee Club | Episode 10 | Ellie Roebuck & Alex Greenwood",
          duration: 1229,
          category: "Feature"
        },
        {
          id: "eb2bbb41-9019-4c04-8e95-90bcbe4204d2",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_h8cwqw7m/",
          title: "One Year On | England vs Cameroon | Steph Houghton, Leah Williamson & Fans",
          duration: 1200,
          category: "Feature"
        },
        {
          id: "6a58f617-8f95-4f1b-9569-539dd8edda9e",
          thumbnail: "https://open.http.mp.streamamg.com/p/3001343/sp/300134300/thumbnail/entry_id/0_m9ap9053/",
          title: "One Year On | England vs Japan | Georgia Stanway, Ellen White, Leah Williamson",
          duration: 770,
          category: "Feature"
        }
      ]
  },
});
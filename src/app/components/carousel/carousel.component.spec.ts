import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CarouselDirectiveComponent } from '../carousel/carousel.directive.component';
import { HeadingThreeComponent } from '../heading-three/heading-three.component';
import { HeadingStrapComponent } from '../heading-strap/heading-strap.component';
import { PreviewMediaComponent } from '../preview-media/preview-media.component';
import { MediaTimeStampComponent } from '../media-time-stamp/media-time-stamp.component';
import { MediaPassIconComponent } from '../media-pass-icon/media-pass-icon.component';
import { HeadingTwoComponent } from '../heading-two/heading-two.component';
import { CarouselComponent } from './carousel.component';
import { FlickityModule } from 'ngx-metafizzy-flickity';

describe('CarouselComponent', () => {
  let component: CarouselComponent;
  let fixture: ComponentFixture<CarouselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        CarouselComponent,
        CarouselDirectiveComponent,
        HeadingThreeComponent,
        HeadingStrapComponent,
        PreviewMediaComponent,
        MediaTimeStampComponent,
        MediaPassIconComponent,
        HeadingTwoComponent
      ],
      imports: [FlickityModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component } from '@angular/core';
import config from './carousel.config';

@Component({
  selector: 'app-carousel-directive',
  templateUrl: './carousel.flickity.html'
})
export class CarouselDirectiveComponent {
  constructor() { }
  config = config;
}

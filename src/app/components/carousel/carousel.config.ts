const config = {
        template: './carousel.flickity.html',
        cellAlign: 'left',
        percentPosition: true,
        groupCells: true,
        adaptiveHeight: true,
        pageDots: false
};

export default config;

import { action } from '@storybook/addon-actions';
import { HeadingTwoComponent } from './heading-two.component';
export default {
  title: 'Components',
};
export const HeadingTwo = () => ({
  component: HeadingTwoComponent,
  props: {
    label: 'Heading Two Label'
  },
});
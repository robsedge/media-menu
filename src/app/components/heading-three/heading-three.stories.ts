import { action } from '@storybook/addon-actions';
import { HeadingThreeComponent } from './heading-three.component';
export default {
  title: 'Components',
};
export const HeadingThree = () => ({
  component: HeadingThreeComponent,
  props: {
    label: 'Heading Three Label'
  },
});
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-heading-three',
  templateUrl: './heading-three.component.html',
  styleUrls: ['./heading-three.component.scss']
})
export class HeadingThreeComponent implements OnInit {
  @Input() label: String;
  constructor() { }

  ngOnInit(): void {
  }

}
